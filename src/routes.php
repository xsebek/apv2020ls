<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');


$app->get('/persons', function (Request $request, Response $response, $args) {
    // Render index view
	//V premennej stmt je ulozny vysleek dotazu jako objekt databazoveho konektoru
	$stmt = $this->db->query('SELECT * FROM person ORDER BY first_name');

	//Je potreba prevest na datovu strukturu php
	$tplVars['osoby'] = $stmt->fetchAll();


    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('persons');

$app->get('/persons/profile', function (Request $request, Response $response, $args) {
    $id_person = $request->getQueryParam('id_person');
    $stmt = $this->db->prepare('SELECT * FROM person WHERE id_person = :id_person');
    $stmt->bindValue(':id_person', $id_person);
    $stmt->execute();
    $tplVars['profil'] = $stmt->fetch();
    return $this->view->render($response, 'editPerson.latte', $tplVars);
})->setName('profile');

// Route for showing form for add new person
$app->get('/persons/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'first_name' => '',
        'last_name' => '',
        'nickname' => '',
        'id_location' => null,
        'gender' => '',
        'height' => '',
        'birth_day' => '',
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => ''
    ];
    return $this->view->render($response, 'newPerson.latte', $tplVars);
})->setName('newPerson');

function insert_location($db, $formData) {
    try {
        $stmt = $db->prepare("INSERT INTO location (city, street_name, street_number, zip) VALUES 
                            (:city, :street_name, :street_number, :zip)");
        $stmt->bindValue(":city", empty($formData['city']) ? null : $formData['city']);
        $stmt->bindValue(":street_number", empty($formData['street_number']) ? null : $formData['street_number']);
        $stmt->bindValue(":street_name", empty($formData['street_name']) ? null : $formData['street_name']);
        $stmt->bindValue(":zip", empty($formData['zip']) ? null : $formData['zip']);
        $stmt->execute();
        return $db->lastInsertId('location_id_location_seq');
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

// Route when data received from form
$app->post('/persons/new', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
        try {
            $stmt = $this->db->prepare("INSERT INTO person (nickname, first_name, last_name, id_location, birth_day, height, gender) 
                        VALUES (:nickname, :first_name, :last_name, :id_location, :birth_day, :height, :gender)");
            $stmt->bindValue(":nickname", $formData['nickname']);
            $stmt->bindValue(":first_name", $formData['first_name']);
            $stmt->bindValue(":last_name", $formData['last_name']);
            $stmt->bindValue(":id_location", $id_location);
            $stmt->bindValue(":gender", $formData['gender'] ? $formData['gender'] : null);
            $stmt->bindValue(":height", $formData['height'] ? $formData['height'] : null);
            $stmt->bindValue(":birth_day", $formData['birth_day'] ? $formData['birth_day'] : null);

            $stmt->execute();
            $tplVars['message'] = 'Person succesfuly inserted';
        } catch (PDOException $exception) {
            $tplVars['message'] = 'Sorry, that person already exists!';
            $this->logger->error($exception->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'newPerson.latte', $tplVars);
});

// Edit user
$app->get('/persons/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('od person is missing');
    } else {
        $stmt = $this->db->prepare('SELECT * FROM person LEFT JOIN location 
                ON (person.id_location = location.id_location) WHERE id_person = :id_person');
        $stmt->bindValue(':id_person', $params['id_person']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('Person not found');
        } else {
            return $this->view->render($response, 'editPerson.latte', $tplVars);
        }
    }
})->setName('editPerson');


$app->post('/persons/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('id person is missing');
    } else {
        try {$stmt = $this->db->prepare("DELETE FROM person WHERE id_person = :id_person");
        $stmt->bindValue(":id_person", $params['id_person']);
        $stmt->execute();
        } catch (PDOException $exception) {
            $this->logger->info($exception);
            exit('Unexpected error occured');
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('persons'));
}) -> setName('person_delete');